DROP TABLE IF EXISTS dbtest_players;
DROP TABLE IF EXISTS dbtest_team;

CREATE TABLE IF NOT EXISTS dbtest_team (
  team_name VARCHAR(70),
  team_id   INT,
  hometown  VARCHAR(70),
  captain   VARCHAR(70),
  points    INT,
  PRIMARY KEY (team_name)
);

CREATE TABLE IF NOT EXISTS dbtest_players (
  player_id   INT,
  first_name  VARCHAR(70),
  last_name   VARCHAR(70),
  age         INT,
  nationality VARCHAR(70),
  team_name   VARCHAR(70),
  PRIMARY KEY (player_id),
  FOREIGN KEY (team_name) REFERENCES dbtest_team (team_name)
);

INSERT INTO dbtest_team VALUES
  ('Arsenal', 1, 'London (Highbury)', 'Thierry Henry (175)', 13),
  ('Aston Villa', 2, 'Birmingham (Aston)', 'Gabby Agbonlahor (73)', 13),
  ('Barnsley', 3, 'Barnsley', 'Neil Redfearn (10)', 13),
  ('Birmingham City', 4, 'Birmingham (Bordesley)', 'Mikael Forssell (29)', 13),
  ('Blackburn Rovers', 5, 'Blackburn', 'Alan Shearer (112)', 13),
  ('Blackpool', 6, 'Blackpool', 'DJ Campbell (13)', 13),
  ('Bolton Wanderers', 7, 'Bolton (Horwich)', 'Kevin Davies (68)', 13),
  ('Bournemouth', 8, 'Bournemouth (Boscombe)', 'Joshua King (22)', 13),
  ('Bradford City', 9, 'Bradford', 'Dean Windass (13)', 13),
  ('Brighton & Hove Albion', 10, 'Brighton (Falmer)', 'Pascal Groß (2)', 13),
  ('Burnley', 11, 'Burnley', 'Sam Vokes (12)', 13),
  ('Cardiff City', 12, ' Cardiff', 'Jordon Mutch (7)', 13),
  ('Charlton Athletic', 13, 'London (Charlton)', 'Jason Euell (34)', 13),
  ('Chelsea', 14, 'London (Fulham)', 'Frank Lampard (147)', 13),
  ('Coventry City', 15, 'Coventry', 'Dion Dublin (61)', 13),
  ('Crystal Palace', 16, 'London (Selhurst)', 'Chris Armstrong (23)', 13),
  ('Derby County', 17, 'Derby', 'Dean Sturridge (32)', 13),
  ('Everton', 18, 'Liverpool (Walton)', 'Romelu Lukaku (68)', 13),
  ('Fulham', 19, 'London (Fulham)', 'Clint Dempsey (50)', 13),
  ('Huddersfield Town', 20, 'Huddersfield', 'Steve Mounié (2)', 13);

INSERT INTO dbtest_players VALUES
(1, 'Vincent','Wen',1,'Australian','Arsenal'),
(2,'Vincent', 'Wen', 2, 'Australian', 'Aston Villa'),
(3, 'Vincent','Wen',3,'Australian','Barnsley'),
(4,'Vincent', 'Wen', 4, 'Australian', 'Birmingham City'),
(5, 'Vincent','Wen',5,'Australian','Blackburn Rovers'),
(6,'Vincent', 'Wen', 6, 'Australian', 'Blackpool'),
(7, 'Vincent','Wen',7,'Australian','Bolton Wanderers'),
(8,'Vincent', 'Wen', 8, 'Australian', 'Bournemouth'),
(9, 'Vincent','Wen',9,'Australian','Bradford City'),
(10,'Vincent', 'Wen', 10, 'Australian', 'Brighton & Hove Albion'),
(11, 'Vincent','Wen',11,'Australian','Burnley'),
(12,'Vincent', 'Wen', 12, 'Australian', 'Cardiff City'),
(13, 'Vincent','Wen',13,'Australian','Charlton Athletic'),
(14,'Vincent', 'Wen', 14, 'Australian', 'Chelsea'),
(15, 'Vincent','Wen',15,'Australian','Coventry City'),
(16,'Vincent', 'Wen', 16, 'Australian', 'Crystal Palace'),
(17, 'Vincent','Wen',17,'Australian','Derby County'),
(18,'Vincent', 'Wen', 18, 'Australian', 'Everton'),
(19, 'Vincent','Wen',19,'Australian','Fulham'),
(20,'Vincent', 'Wen', 20, 'Australian', 'Huddersfield Town');

