-- Answers to exercise 2 questions
DROP TABLE IF EXISTS db_players;
DROP TABLE IF EXISTS db_team;

CREATE TABLE IF NOT EXISTS db_team (
  team_id   VARCHAR(2),
  team_name VARCHAR(50),
  home      VARCHAR(50),
  captain   VARCHAR(50),
  points    INT NOT NULL,
  PRIMARY KEY (team_id)
);

CREATE TABLE IF NOT EXISTS db_players (
  player_id   VARCHAR(2),
  player_name VARCHAR(50),
  team_name   VARCHAR(50),
  age         INT,
  nationality VARCHAR(70),
  PRIMARY KEY (player_id),
  FOREIGN KEY (team_name) REFERENCES db_team (team_name)
);





